import scala.io.Source

object Main {

  case class Track(title: String = "", length: String = "", rating: Int = 0, features: List[String] = List(), writers: List[String] = List())

  case class Album(title: String = "", date: String = "", artist: String = "", tracks: List[Track] = List())

  /**
    * Parse XML-File given as characterlist to list of tokens(String)
    * @param content  :   filecontent as characterlist
    * @param tokens   :   list of found tokenstrings
    * @return tokens as list of strings
    */
  def createTokenList(content: List[Char], tokens: List[String] = List()): List[String] = content match {
    case Nil => tokens
    case ('\r'|'\n'|'\t') :: tail => createTokenList(tail, tokens)
    case '<'  :: tail =>
      val token: (List[Char], String) = getToken(tail, '>', "")
      createTokenList(token._1.tail, tokens :+ token._2)
    case _  =>
      val token: (List[Char], String) = getToken(content, '<', "")
      createTokenList(token._1, tokens :+ token._2)
  }

  /**
    * Parse single token from characterlist, delimited by pattern
    * @param content  : characterlist to parse
    * @param pattern  : delimiter patter for token
    * @param token    : actual token
    * @return tupel of remaining characterlist and found token
    */
  def getToken(content: List[Char], pattern: Char, token: String): (List[Char], String) = content match {
    case Nil => (content, token)
    case `pattern` :: _ => (content, token)
    case _ => getToken(content.tail, pattern, token + content.head)
  }

  /**
    * Parse list of tokenstrings to list of albums
    * @param tokenList  : list of tokenstrings
    * @param albums     : actual list of albums
    * @return list of albums
    */
  def parseTokenList(tokenList: List[String], albums: List[Album] = List()): List[Album] = tokenList match {
    case Nil => albums
    case "album" :: tail =>
      val album: (List[String], Album) = createAlbum(tail)
      parseTokenList(album._1, albums :+ album._2)
    case _ :: tail => parseTokenList(tail, albums)
  }

  /**
    * Parse tokenstrings to single album
    * @param tokenList  : list of tokenstrings
    * @param album      : actual album
    * @return tupel of remaining list of tokenstrings and parsed album
    */
  def createAlbum(tokenList: List[String], album: Album = Album(), tag: String = ""): (List[String], Album) = tokenList match {
    case Nil => (tokenList, album)
    case "/album" :: tail => (tail, album)
    case "title" :: tail => createAlbum(tail, album, "title")
    case "/title" :: tail => createAlbum(tail, album)
    case "artist" :: tail => createAlbum(tail, album, "artist")
    case "/artist" :: tail => createAlbum(tail, album)
    case "date" :: tail => createAlbum(tail, album, "date")
    case "/date" :: tail => createAlbum(tail, album)
    case "track" :: tail =>
      val track: (List[String], Track) = createTrack(tail)
      createAlbum(track._1, album.copy(tracks = album.tracks :+ track._2))
    case head :: tail => tag match {
      case "title" => createAlbum(tail, album.copy(title = head))
      case "artist" => createAlbum(tail, album.copy(artist = head))
      case "date" => createAlbum(tail, album.copy(date = head))
      case _ => createAlbum(tail, album, tag)
    }
  }

  /**
    * Parse tokenstrings to single track
    * @param tokenList  : list of tokenstrings
    * @param track      : actual track
    * @return tupel of remaining list of tokenstrings and parsed track
    */
  def createTrack(tokenList: List[String], track: Track = Track()): (List[String], Track) = tokenList match {
    case Nil => (tokenList, track)
    case "/track" :: tail => (tail, track)
    case "title" :: x1 :: "/title" :: tail => createTrack(tail, track.copy(title = x1))
    case "length" :: tail => createTrack(tail.tail.tail, track.copy(length = tail.head))
    case "rating" :: tail => createTrack(tail.tail.tail, track.copy(rating = tail.head.toInt))
    case "feature" :: tail => createTrack(tail.tail.tail, track.copy(features = track.features :+ tail.head))
    case "writing" :: tail => createTrack(tail.tail.tail, track.copy(writers = track.writers :+ tail.head))
    case _ :: tail => createTrack(tail, track)
  }


  /**
    * Main entry method
    * @param args
    */
  def main(args: Array[String]): Unit = {
    println("Praktikum 2 - Aufgabe 2-3")

    val chars: List[Char] = Source.fromFile("alben.xml").mkString.toCharArray.toList
    println(chars)
    val tokenList: List[String] = createTokenList(chars)
//    printTokenList(tokenList)

    val alben: List[Album] = parseTokenList(tokenList)
//    println(alben)

    // ####################################
    // Praktikum 3 - Aufgabe 1
    val thriller : Album = alben.apply(1)

    // 1b
    println("A 1b")
    println( map[Album](thriller::Nil, album => album.copy(
      title = album.title.toUpperCase)) )

    // 1c
    println("A 1c")
    println( map[Album](thriller::Nil, album => album.copy(
      title = album.title.toUpperCase(),
      tracks = map[Track](album.tracks, track => track.copy(track.title.toUpperCase())))) )

    // 1e
    println("A 1e")
    println( poly_map[Album, List[String]](thriller::Nil, album => poly_map[Track, String](album.tracks, track => track.length)) )

    // 2b
    println("A 2b")
    println( filter[Track](thriller.tracks, track => track.rating >= 4))

    // 2c
    println("A 2c")
    println( poly_map[Track, String](
      filter[Track](thriller.tracks,
        track => filter[String](track.writers, writer => writer == "Rod Temperton") != Nil),
      track => track.title))

    // 3a test
    println("A 3a test")
    val testList: List[Char] = 'a'::'b'::'c'::'D'::'e'::'f'::'G'::'H'::'i'::'J'::Nil
    println( partition[Char](testList, char => char.isUpper ))

    // 3b
    println("A 3b")
    println( partition[Track](thriller.tracks, track => track.title == "Thriller"))

    // 3c
    println("A 3c")
    println(
      poly_map[List[Char], String](
        filter[List[Char]](
          partition[Char](
            chars, c => c == '<' | c == '>' | c == '\n' | c == '\r' | c == '\t'),
          c => c != List() ),
        charL => charL.mkString) )

    // 4a
    println("A 4")
    println("Sum: " + composite( (x, y) => x + y, z => z * z, 1, 3, 0) ) // 14
    println("Prod: " + composite( (x, y) => x * y, z => z * z, 1, 3, 1) ) // 36

    // 4b
    println("A 4b")
    println( composite2((x,y) => x+y, z => z*z, 1, 5, 0) )

  }

  def printTokenList(list: List[String]): Boolean = list match {
    case Nil => print('\n'); true
    case head :: tail  =>
      print(head + ", ")
      printTokenList(tail)
  }


  // ###################################################################################################################
  //
  // Praktikum 3
  //
  // ###################################################################################################################


  // 1a
  def map[A](list: List[A], func: A => A, res: List[A] = List()) : List[A] = list match {
    case Nil => res
    case head :: tail => map[A](tail, func, res :+ func(head))
  }

  // 1d
  def poly_map[A,B](list: List[A], func: A => B, res: List[B] = List()) : List[B] = list match {
    case Nil => res
    case head :: tail => poly_map[A, B](tail, func, res :+ func(head))
  }

  // 2a
  def filter[A](list: List[A], condition: A => Boolean, res: List[A] = List()) : List[A] = list match {
    case Nil => res
    case head :: tail if condition(head) => filter[A](tail, condition, res :+ head)
    case _ :: tail => filter[A](tail, condition, res)
  }

  // 3a
  def partition[A](list : List[A],
                   condition: A => Boolean,
                   resOuter: List[List[A]] = List(),
                   resInner: List[A] = List()) : List[List[A]] = list match {
    case Nil => resOuter :+ resInner
    case head :: tail if condition(head) => partition[A](tail, condition, resOuter :+ resInner)
    case head :: tail => partition[A](tail, condition, resOuter, resInner :+ head)
  }

  // 4a
  def composite(f: (Int, Int) => Int, g: Int => Int, a: Int, b: Int, res:Int) : Int = b match {
    case _ if b == a => f(res, g(a))
    case _ if b > a => composite(f, g, a + 1, b, f(res, g(a)))
    case _ => composite(f, g, a, b + 1, f(res, g(b)))
  }


  def fold(list: List[Int], func: (Int, Int) => Int, res: Int) : Int = list match {
    case Nil => res
    case head::tail => func(head, fold(tail, func, res))
  }

  def range(a: Int, b: Int, list: List[Int] = List()) : List[Int] = b match {
    case _ if b == a => list :+ a
    case _ if b > a => range(a + 1, b, list :+ a)
    case _ => range(b, a)
  }

  def composite2(f_fold: (Int, Int) => Int, f_map: Int => Int, a: Int, b: Int, res:Int) : Int =
    fold( map[Int]( range(a,b), f_map), f_fold, res)
}



